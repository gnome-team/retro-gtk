Source: retro-gtk
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               dh-sequence-gnome,
               gtk-doc-tools (>= 1.24-2),
               libgirepository1.0-dev,
               libglib2.0-dev (>= 2.44),
               libgtk-3-dev (>= 3.20),
               libpulse-dev,
               libsamplerate0-dev,
               meson (>= 0.50.0),
               valac,
               xauth <!nocheck>,
               xvfb <!nocheck>
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/gnome-team/retro-gtk
Vcs-Git: https://salsa.debian.org/gnome-team/retro-gtk.git
Homepage: https://wiki.gnome.org/Apps/Games

Package: libretro-gtk-1-0
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         retro-runner (= ${binary:Version})
Description: library for GTK+ libretro frontends
 retro-gtk is a library for developers to make GTK+ frontends for the libretro
 API.
 .
 libretro is an API for the creation of games and emulators. The emulators
 and platforms are distributed as "libretro cores".
 .
 Therefore, a developer can use retro-gtk to make a gaming app that should
 be able to play any game that can be played using libretro cores.

Package: libretro-gtk-1-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         gir1.2-retro-1 (= ${binary:Version}),
         libretro-gtk-1-0 (= ${binary:Version}),
         libgtk-3-dev
Breaks: libretro-gtk-0.10-dev,
        libretro-gtk-0.12-dev,
        libretro-gtk-0.14-dev
Replaces: libretro-gtk-0.10-dev,
          libretro-gtk-0.12-dev,
          libretro-gtk-0.14-dev
Description: library for GTK+ libretro frontends - development files
 retro-gtk is a library for developers to make GTK+ frontends for the libretro
 API.
 .
 libretro is an API for the creation of games and emulators. The emulators
 and platforms are distributed as "libretro cores".
 .
 Therefore, a developer can use retro-gtk to make a gaming app that should
 be able to play any game that can be played using libretro cores.
 .
 This package contains the development files.

Package: gir1.2-retro-1
Section: introspection
Architecture: linux-any
Multi-Arch: same
Depends: ${misc:Depends},
         ${gir:Depends}
Description: GObject introspection data for retro-gtk
 This package contains GObject introspection information.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.

Package: retro-runner
Architecture: linux-any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: background service for retro-gtk
 retro-gtk is a library for developers to make GTK+ frontends for the libretro
 API.
 .
 libretro is an API for the creation of games and emulators. The emulators
 and platforms are distributed as "libretro cores".
 .
 Therefore, a developer can use retro-gtk to make a gaming app that should
 be able to play any game that can be played using libretro cores.
 .
 This package contains the background service.
